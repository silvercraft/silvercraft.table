using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Avalonia.Collections;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Threading;
using DynamicData;
using ReactiveUI;
using STable.Shared;

namespace STable.Editor;

public partial class MainWindow : Window
{
    MainWindowContext mainWindowContext = new();
    public MainWindow()
    {
        Task.Run(async () =>
        {
            var res = await FieldGatherer.GatherFieldsAsync();
            if (res is { } useful)
            {
                Dispatcher.UIThread.InvokeAsync(() =>
                {
                    mainWindowContext.Fields = useful;
                });
            }
        }).GetAwaiter().GetResult();
        DataContext = mainWindowContext;
        InitializeComponent();
    }

    private void addButtonPressed(object? sender, RoutedEventArgs e)
    {
        if (sender is Button b && b.DataContext is AvaloniaList<Field> f)
        {
            f.Add(new("new field"));
        }
    }
    private void removeButtonPressed(object? sender, RoutedEventArgs e)
    {
        if (sender is Button b && b.DataContext is AvaloniaList<Field> f && f.Count>0)
        {
            f.RemoveAt(f.Count-1);
        }
    }
    private void SaveClick(object? sender, RoutedEventArgs e)
    {
        var s = new XmlSerializer(typeof(List<List<Field>>));
        List<List<Field>> a = new();

        foreach (var r in mainWindowContext.Fields)
        {
            a.Add([.. r]);
        }

        Utf8StringWriter sw = new();
        s.Serialize(sw, a);
        Clipboard.SetTextAsync(sw.ToString());
    }
}
public class Utf8StringWriter :StringWriter
{
    public override Encoding Encoding => Encoding.UTF8;
}
public class MainWindowContext : ReactiveObject
{
    public AvaloniaList<AvaloniaList<Field>> Fields { get => _Fields; set => this.RaiseAndSetIfChanged(ref _Fields, value); }
    
    private AvaloniaList<AvaloniaList<Field>> _Fields = new()
            {
                new()
                {
                    new("I", bold: true), new("07:30 – 08:15"), new("08:15 – 09:00"), new("09:00 – 09:45"),
                    new("10:05 – 10:50"), new("10:50 – 11:35"), new("11:35 – 12:20"), new("12:20 – 13:05")
                },
                new()
                {
                    new("II", bold: true), new("13:20 – 14:05"), new("14:05 – 14:50"), new("14:50 – 15:35"),
                    new("15:55 – 16:40"), new("16:40 – 17:25"), new("17:25 – 18:10"), new("18:10 – 18:55")
                },
                new()
                {
                    new("Понеделник", bold: true), 
                },
                new()
                {
                    new("Вторник", bold: true),
                },
                new()
                {
                    new("Среда", bold: true), 
                },
                new()
                {
                    new("Четврток", bold: true),
                },
                new()
                {
                    new("Петок", bold: true), 
                },
            };
   
}