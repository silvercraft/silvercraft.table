using System;
using System.Globalization;
using Avalonia.Data;
using Avalonia.Data.Converters;

namespace STable.Editor;

public class RowBindingConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value == null) return null;
        if (value is string sourceText 
                                       && targetType.IsAssignableTo(typeof(int?)))
        {
            if (int.TryParse(sourceText, out int result))
            {
                return result;
            }

            return null;
        }
        if (value is int source 
            && targetType.IsAssignableTo(typeof(string)))
        {
           

            return source.ToString();
        }
        return new BindingNotification(new InvalidCastException(), 
            BindingErrorType.Error);
    }
    

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        return Convert(value, targetType, parameter, culture);
    }
}