# STable.Editor

## What is this?
Sometimes you've got to edit the timetable, and aren't in the state to edit a xml file.  
This grabs the latest xml, lets you edit it and then copy the edited version to your clipboard (where you can then slap it into a text editor save that, brotli encode it and bobs your uncle)

## Known bugs
Scales like AWFUL, GOOD LUCK RUNNING ON SOMETHING LESS THAN A 4K 27 inch display  

## Wait so why are there so many textboxes and what does the checkbox do?
So you've got the name (biggest textbox), room id (top right), whether the field is bold (checkbox) and a rowspan at the bottom

## Whats a rowspan?
Sometimes youve got the same subject twice, instead of doing

| Monday | Tuesday |
|--------|---------|
| Math   | Science |
| Math   | Sport   |

You can do

<table>
<tr><td>Monday</td><td>Tuesday</td></tr>
<tr><td rowspan="2">Math</td><td>Science</td></tr>
<tr><td>Sport</td></tr>
</table>

Where the rowspan is how many cells it takes up

You set the rowspan only on the first occurrence of the subject, then leave the rowspan blank for all other occurrences in that span.
