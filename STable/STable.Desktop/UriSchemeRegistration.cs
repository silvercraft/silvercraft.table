using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.Versioning;
using SilverCraft.AvaloniaUtils;

namespace STable.Desktop;

public static class RegistryRegistration
{
    public static void RegisterUrlScheme()
    {
        if (OperatingSystem.IsLinux())
        {
            RegisterURLSchemeLinux();
        }
    }

    public static void UnRegisterUrlScheme()
    {
        if (OperatingSystem.IsLinux())
        {
            UnRegisterUrlSchemeLinux();
        }
    }
    [SupportedOSPlatform("linux")]

    private static void UnRegisterUrlSchemeLinux()
    {
        File.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
            ".local/share/applications/stable.desktop"));
        DoOrAsk("rm /usr/share/pixmaps/stable.svg");
    }

 


    public static bool? IsReg()
    {
        if (OperatingSystem.IsLinux())
        {
            return File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                ".local/share/applications/stable.desktop"));
        }
        return null;
    }

    [SupportedOSPlatform("linux")]
    public static void RegisterURLSchemeLinux()
    {
        var f = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
            ".local/share/applications/stable.desktop");
        if (File.Exists(f)) return;
        File.WriteAllText(f, @$"[Desktop Entry]
Comment[en_GB]=The SilverCraft timetable app
Comment=The SilverCraft timetable app
Path={AppContext.BaseDirectory}
Exec={Environment.ProcessPath} %U
GenericName[en_GB]=Timetable app
GenericName=TimeTable app
Icon=stable
Keywords=Timetable;Schedule;Dotnet;
Name[en_GB]=STable
Name=STable
NoDisplay=false
StartupNotify=true
Terminal=false
TerminalOptions=
Type=Application
X-KDE-Protocols=stable
X-KDE-SubstituteUID=false
Categories=Education;Dotnet;");
        using var iconFile = File.OpenWrite("/tmp/stable.svg");
        using var iconSource = typeof(ViewLocator).Assembly
            .GetManifestResourceStream("STable.Assets.icon.svg");
        iconSource.CopyTo(iconFile);
        iconSource.Close();
        iconFile.Close();
        DoOrAsk("mv /tmp/stable.svg /usr/share/pixmaps/stable.svg");
    }

    [SupportedOSPlatform("linux")]
    static void DoOrAsk(string command)
    {
        if (File.Exists("/usr/bin/kdesu"))
        {
            Process.Start("kdesu", command);
        }
        else if (File.Exists("/usr/bin/pkexec"))
        {
            Process.Start("pkexec", command);
        }
        else
        {
            MessageBox m = new("Additional setup required",
                $"Please run {command} as root in a terminal");
            m.Show();
        }
    }

}