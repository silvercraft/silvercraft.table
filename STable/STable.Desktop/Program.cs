﻿using System;
using System.Linq;
using Avalonia;
using Avalonia.ReactiveUI;

namespace STable.Desktop;

class Program
{
    // Initialization code. Don't use any Avalonia, third-party APIs or any
    // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
    // yet and stuff might break.
    [STAThread]
    public static void Main(string[] args)
    {
        PlatformInfoHolder.Platform = new NotWatchPlatformInfo();
        PlatformInfoHolder.Reader = new DesktopFileReaderReader();
        if (args.Contains("reg"))
        {
            RegistryRegistration.RegisterUrlScheme();
        }
        BuildAvaloniaApp()
            .StartWithClassicDesktopLifetime(args);
    }

    // Avalonia configuration, don't remove; also used by visual designer.
    public static AppBuilder BuildAvaloniaApp()
        => AppBuilder.Configure<App>()
            .UsePlatformDetect()
            .LogToTrace()
            .UseReactiveUI();
}