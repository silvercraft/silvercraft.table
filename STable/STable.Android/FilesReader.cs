﻿using Xamarin.Essentials;

namespace STable.Android
{
    public class FilesReader : FileReader
    {
        public override string BaseDir =>  FileSystem.AppDataDirectory;
    }
}