using System;
using System.Runtime.CompilerServices;
using Android.Content.Res;
using Android.OS;
using Plugin.CurrentActivity;
using Xamarin.Essentials;

namespace STable.Android;
public class AndroidPlatformInfo : IPlatformInfo
{
    public bool IsWatch => DeviceInfo.Idiom == DeviceIdiom.Watch;
    public Theme Theme =>  GetOperatingSystemTheme();
    public Theme GetOperatingSystemTheme()
    {
        if (Build.VERSION.SdkInt < BuildVersionCodes.Froyo || DeviceInfo.Idiom == DeviceIdiom.Watch ||
            CrossCurrentActivity.Current.AppContext.Resources == null) return Theme.Dark;
        var uiModeFlags = CrossCurrentActivity.Current.AppContext.Resources.Configuration.UiMode & UiMode.NightMask;
        return uiModeFlags switch
        {
            UiMode.NightNo => Theme.Light,
            UiMode.NightYes => Theme.Dark,
            _ => Theme.Dark
        };

    }
}