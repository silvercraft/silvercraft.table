﻿using Android.App;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Avalonia;
using Avalonia.Android;
using Avalonia.ReactiveUI;
using Plugin.CurrentActivity;

namespace STable.Android;
[Activity(
    Label = "STable",
    Theme = "@style/MyTheme.NoActionBar",
    Icon = "@drawable/icon",
    MainLauncher = true,
    ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.UiMode)]
public class MainActivity : AvaloniaMainActivity<App>
{
    public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
    {
        Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    protected override void OnCreate(Bundle? savedInstanceState)
    {
        CrossCurrentActivity.Current.Init(this, savedInstanceState);
        base.OnCreate(savedInstanceState);
        Window.AddFlags(WindowManagerFlags.ShowWallpaper);
        Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);  
        Window.SetStatusBarColor(Color.Transparent);  
        Window.Attributes.LayoutInDisplayCutoutMode = LayoutInDisplayCutoutMode.ShortEdges;
    }
    protected override AppBuilder CustomizeAppBuilder(AppBuilder builder)
    {
        PlatformInfoHolder.Platform = new AndroidPlatformInfo();
        PlatformInfoHolder.Reader = new FilesReader();
        return base.CustomizeAppBuilder(builder)
            .WithInterFont()
            .UseReactiveUI();
    }
}
