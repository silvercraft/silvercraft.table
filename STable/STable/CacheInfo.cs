
using System;
using System.Xml.Serialization;

namespace STable;

public class CacheInfo
{
   public CacheInfo(DateTime lastUpdated)
   {
      LastUpdated = lastUpdated;
   }
   public CacheInfo() { }
   public DateTime LastUpdated { get; set; }
   public static XmlSerializer XmlSerializer => new XmlSerializer(typeof(CacheInfo));
   public static string FileName = "CacheInfo.xml";
}