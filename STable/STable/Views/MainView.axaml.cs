using Avalonia.Controls;
using ReactiveUI;

namespace STable.Views
{
   
    public partial class MainView : UserControl
    {
        public MainView()
        {
            InitializeComponent();
            _timetable = new Content(this);
            _settings= new Settings(this);
            Content = _timetable;
        }

        private Content _timetable;
        private Settings _settings;
        public void SwitchContent(bool settings)
        {
            Content = settings ? _settings : _timetable;
        }
    }
}