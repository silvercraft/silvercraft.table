using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using STable.ViewModels;

namespace STable.Views;

public partial class Settings : UserControl
{
    private readonly MainView _mainView;

    public Settings(MainView mainView)
    {
        _mainView = mainView;
        InitializeComponent();
        var dc = new SettingsViewModel();
        DataContext = dc;


        dc.FileReader = PlatformInfoHolder.Reader;
        SettingsFile file = SettingsFile.Load(dc.FileReader);
        URLBox.Text = file.URL;
        CacheValidityPicker.SelectedTime = file.CacheValidity;
    }

    private void Button_OnClick(object? sender, RoutedEventArgs e)
    {
        SettingsFile file = new SettingsFile()
        {
            URL = URLBox.Text,
            CacheValidity = CacheValidityPicker.SelectedTime??TimeSpan.Zero,
        };
        SettingsFile.Save(PlatformInfoHolder.Reader, file);
        _mainView.SwitchContent(false);
        
    }
}