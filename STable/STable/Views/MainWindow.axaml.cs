using Avalonia.Controls;
using SilverCraft.AvaloniaUtils;

namespace STable.Views;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
        this.DoAfterInitTasks(true);
    }
}