﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Media;
using Avalonia.Threading;
using STable.ViewModels;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.Net.Http;
using System.Threading.Tasks;
using Avalonia.Interactivity;
using Avalonia.Layout;
using STable.Shared;

namespace STable.Views
{
    public partial class Content : UserControl
    {
        SolidColorBrush AzureBrush = new(Colors.Azure);
        SolidColorBrush WhiteBrush = new(Colors.White);
        SolidColorBrush BlackBrush = new(Colors.Black);

        SolidColorBrush AquaBrush = new(Colors.Aqua);
        Thickness BorderThickness = new(2);
        private MainView mainView;
        public Content(MainView mainView)
        {
            this.mainView = mainView;
            InitializeComponent();
            mainGrid = this.FindControl<Grid>("mainGrid") ?? throw new Exception("MainGrid returned null");
            var platform = PlatformInfoHolder.Platform;
            Button settingsButton = new()
            {
                Content = "Settings",
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
            };
            settingsButton.Classes.AddRange(["h1","c0", "r0"]);
            Grid.SetColumnSpan(settingsButton, 2);
            settingsButton.Click += (s, e) =>
            {
                mainView.SwitchContent(true);
            };
            mainGrid.Children.Add(settingsButton);
            if (PlatformInfoHolder.Platform.Theme == STable.Theme.Light)
            {
                (WhiteBrush,BlackBrush) = (BlackBrush,WhiteBrush);
            }
            var dc = new MainViewModel
            {
                FC = WhiteBrush,
                BC = null
            };
            DataContext = dc;
            dc.FileReader = PlatformInfoHolder.Reader;


            List<List<Field>> values = new()
            {
                new()
                {
                    new("I", bold: true), new("07:30 – 08:15"), new("08:15 – 09:00"), new("09:00 – 09:45"),
                    new("10:05 – 10:50"), new("10:50 – 11:35"), new("11:35 – 12:20"), new("12:20 – 13:05")
                },
                new()
                {
                    new("II", bold: true), new("13:20 – 14:05"), new("14:05 – 14:50"), new("14:50 – 15:35"),
                    new("15:55 – 16:40"), new("16:40 – 17:25"), new("17:25 – 18:10"), new("18:10 – 18:55")
                },
                new()
                {
                    new("Понеделник", bold: true), 
                },
                new()
                {
                    new("Вторник", bold: true)
                },
                new()
                {
                    new("Среда", bold: true), 
                },
                new()
                {
                    new("Четврток", bold: true),
                },
                new()
                {
                    new("Петок", bold: true),
                },
            };
            HttpClient c = new();
            DateTime? lastUpdated = null;
            SettingsFile settings = SettingsFile.Load(dc.FileReader);
            if (dc?.FileReader?.FileExists(CacheInfo.FileName) == true)
            {
                using var stringreader=new StringReader(dc.FileReader.ReadFile(CacheInfo.FileName));
                var cacheInfo = CacheInfo.XmlSerializer.Deserialize(stringreader) as CacheInfo;
                lastUpdated=cacheInfo.LastUpdated;
            }

            if (lastUpdated == null || lastUpdated + settings.CacheValidity < DateTime.Now)
            {
                Task.Run(async () =>
                {
                    var tvalues = await FieldGatherer.GatherFieldsAsyncN(settings.URL);
                    if (tvalues != null)
                    {
                        var s = new XmlSerializer(typeof(List<List<Field>>));
                        values = tvalues;
                        StringWriter tx = new();
                        s.Serialize(tx, tvalues);
                        dc.FileReader?.WriteFile("STable.xml", tx.ToString());

                        StringWriter tx2 = new();
                        CacheInfo.XmlSerializer.Serialize(tx2,new CacheInfo(DateTime.Now));
                        dc.FileReader?.WriteFile(CacheInfo.FileName, tx2.ToString());

                    }
                }).GetAwaiter().GetResult();
            }
            


            if (dc?.FileReader?.FileExists("STable.xml") == true)
            {
                var s = new XmlSerializer(typeof(List<List<Field>>));
                values = (List<List<Field>>)s.Deserialize(
                    XmlReader.Create(new StringReader(dc.FileReader.ReadFile("STable.xml"))));
            }
            

            TextBlock CreateTB(string? text = "", bool bold = false, params string[] classes)
            {
                TextBlock tb = new() { Text = text, FontWeight = bold ? FontWeight.Bold : FontWeight.Normal };
                tb.Classes.AddRange(classes);
                return tb;
            }

            Border CreateB(IBrush borderBrush, Thickness borderThickness, params string[] classes)
            {
                Border tb = new() { BorderBrush = borderBrush, BorderThickness = borderThickness };
                tb.Classes.AddRange(classes);
                return tb;
            }

            var maxColumns = values.Count;
            var maxRows = values.Max(v => v.Count);
            mainGrid.ColumnDefinitions = new();
            while (maxColumns-- > 0)
            {
                mainGrid.ColumnDefinitions.Add(new(){Width = new GridLength(1, GridUnitType.Star)});
            }
            mainGrid.RowDefinitions = new();
            while (maxRows-- > 0)
            {
                mainGrid.RowDefinitions.Add(new(){Height = new GridLength(1, GridUnitType.Star)});
            }
            List<Tuple<TimeSpan, TimeSpan, TextBlock, TextBlock, TextBlock>> I = new();
            List<Tuple<TimeSpan, TimeSpan, TextBlock, TextBlock, TextBlock>> II = new();
            var yo = 1;

            void HandleTime(int which)
            {
                yo = 1;
                foreach (var h in values[which].Skip(1))
                {
                    var j = h.Name.Split(new string[] { " – " }, StringSplitOptions.None);
                    var x1 = TimeSpan.Parse(j[0]);
                    var x2 = TimeSpan.Parse(j[1]);
                    var tb = CreateTB(h.Name, h.Bold, "h1", $"c{which}", $"r{yo}");
                    mainGrid.Children.Add(tb);
                    mainGrid.Children.Add(CreateB(AzureBrush, BorderThickness, "h1", $"c{which}", $"r{yo}"));
                    var to1 = CreateTB("", h.Bold, "h3", $"c{which}", $"r{yo}");
                    mainGrid.Children.Add(to1);
                    var to2 = CreateTB("", h.Bold, "h4", $"c{which}", $"r{yo}");
                    mainGrid.Children.Add(to2);
                    yo++;
                    if (which == 0)
                    {
                        I.Add(new(x1, x2, tb, to1, to2));
                    }
                    else
                    {
                        II.Add(new(x1, x2, tb, to1, to2));
                    }
                }

                yo = 1;
            }

            HandleTime(0);
            HandleTime(1);

            Dictionary<DayOfWeek, Tuple<TextBlock, Border>> Days = new();
            var ayo = DayOfWeek.Monday;
            for (var x = 2; x < 7; x++)
            {
                if (values.Count - 1 < x) continue;
                var xo = values[x];
                for (var y = 0; y < 8; y++)
                {
                    if (xo.Count - 1 < y) continue;
                    TextBlock tb = CreateTB(xo[y].Name, xo[y].Bold, "h1", $"c{x}", $"r{y}");
                    if (xo[y].RowSpan is { } i)
                    {
                        Grid.SetRowSpan(tb, i);
                    }

                    mainGrid.Children.Add(tb);
                    Border b = CreateB(AzureBrush, BorderThickness, "h1", $"c{x}", $"r{y}");
                    mainGrid.Children.Add(b);
                    if (xo[y].RowSpan is { } o)
                    {
                        Grid.SetRowSpan(b, o);
                    }

                    if (y == 0)
                    {
                        Days.Add(ayo, new(tb, b));
                        ayo++;
                        continue;
                    }

                    TextBlock? tb2 = null;
                    if (xo[y].RoomID == null) continue;
                    tb2 = CreateTB(xo[y].RoomID, false, "h2", $"c{x}", $"r{y}");    
                    if (xo[y].RowSpan is { } p)
                    {
                        Grid.SetRowSpan(tb, p);
                        y += p - 1;
                    }

                    mainGrid.Children.Add(tb2);
                }
            }

            t = new(TimeSpan.FromSeconds(1), DispatcherPriority.Render, (x, y) =>
            {
                foreach (var p in Days)
                {
                    p.Value.Item1.Foreground = p.Key == DateTime.Now.DayOfWeek ? AquaBrush : WhiteBrush;
                }

                if (DateTime.Now.TimeOfDay > I[0].Item1 && DateTime.Now.TimeOfDay < I[^1].Item2)
                {
                    if (GoofyAahII)
                    {
                        foreach (var i in II)
                        {
                            i.Item4.Text = "";
                            i.Item5.Text = "";
                            i.Item3.Foreground = WhiteBrush;
                        }

                        GoofyAahII = false;
                    }

                    //first
                    foreach (var i in I)
                    {
                        i.Item4.Text = TimeStamp(i.Item1 - DateTime.Now.TimeOfDay);
                        i.Item5.Text = TimeStamp(i.Item2 - DateTime.Now.TimeOfDay);
                        if (i.Item4.Text[0] == '-' && i.Item5.Text[0] != '-')
                        {
                            i.Item3.Foreground = AquaBrush;
                        }
                        else
                        {
                            i.Item3.Foreground = WhiteBrush;
                        }
                    }

                    GoofyAahI = true;
                }
                else if (DateTime.Now.TimeOfDay > II[0].Item1 && DateTime.Now.TimeOfDay < II[^1].Item2)
                {
                    //second
                    if (GoofyAahI)
                    {
                        foreach (var i in I)
                        {
                            i.Item4.Text = "";
                            i.Item5.Text = "";
                            i.Item3.Foreground = WhiteBrush;
                        }

                        GoofyAahI = false;
                    }

                    foreach (var i in II)
                    {
                        i.Item4.Text = TimeStamp(i.Item1 - DateTime.Now.TimeOfDay);
                        i.Item5.Text = TimeStamp(i.Item2 - DateTime.Now.TimeOfDay);
                        if (i.Item4.Text[0] == '-' && i.Item5.Text[0] != '-')
                        {
                            i.Item3.Foreground = AquaBrush;
                        }
                        else
                        {
                            i.Item3.Foreground = WhiteBrush;
                        }
                    }

                    GoofyAahII = true;
                }
            });
            t.Start();
            Unloaded += MainView_Unloaded;
            Loaded += LoadedV;

        }

        private void LoadedV(object? sender, RoutedEventArgs e)
        {
            t.Start();
        }

        bool GoofyAahI = false;
        bool GoofyAahII = false;

        string TimeStamp(TimeSpan x)
        {
            if (x.TotalSeconds < 0)
            {
                return "-" + x.ToString("hh\\:mm\\:ss");
            }

            return x.ToString("hh\\:mm\\:ss");
        }

        DispatcherTimer t;

        private void MainView_Unloaded(object sender, Avalonia.Interactivity.RoutedEventArgs e)
        {
            t.Stop();
        }
    }
}