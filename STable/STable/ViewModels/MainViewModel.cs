﻿using Avalonia.Media;
using ReactiveUI;

namespace STable.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public int Size => 10;
        
        public IFileReader? FileReader;
        private IBrush _FC = null;

        public IBrush FC { get => _FC; set => this.RaiseAndSetIfChanged(ref _FC, value); }
        private IBrush _BC = new SolidColorBrush(Colors.Transparent);

        public IBrush? BC { get => _BC; set => this.RaiseAndSetIfChanged(ref _BC, value); }
     
     
    }
    public class SettingsViewModel : ViewModelBase
    {
        
        public IFileReader? FileReader;
     
    }

}
