﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace STable
{
    public interface IFileReader
    {
        public string ReadFile(string f);
        public Stream ReadStream(string f);


        public bool FileExists(string f);


        public void WriteFile(string f, string data);
        public Stream WriteStream(string f);

        
    }

    public class FileReader : IFileReader
    {
        public virtual string BaseDir { get; }
        private string GetFilePath(string file)
        {
            return Path.Combine(BaseDir, file);
        }
        public string ReadFile(string f) => File.ReadAllText(GetFilePath(f));

        public Stream ReadStream(string f) => File.OpenRead(GetFilePath(f));

        public bool FileExists(string f) => File.Exists(GetFilePath(f));

        public void WriteFile(string f, string data) => File.WriteAllText(GetFilePath(f), data);

        public Stream WriteStream(string f) => new FileStream(GetFilePath(f), FileMode.Create, FileAccess.Write, FileShare.None);
    }
}
