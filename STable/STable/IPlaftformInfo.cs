namespace STable;

public interface IPlatformInfo
{
    public bool IsWatch { get; }
    public Theme Theme{ get; }
}
public enum Theme { Light, Dark }
public class NotWatchPlatformInfo :IPlatformInfo
{
    public bool IsWatch => false;
    public Theme Theme => Theme.Dark;
}

public static class PlatformInfoHolder
{
    public static IPlatformInfo Platform { get; set; } = new NotWatchPlatformInfo();
    public static IFileReader? Reader { get; set; }
}