using System;
using System.Diagnostics;
using System.Xml.Serialization;

namespace STable;

public class SettingsFile
{
    public TimeSpan CacheValidity { get; set; }= TimeSpan.FromMinutes(40);
    public string URL { get; set; } = "https://silverwebsiterepo.pages.dev/schooltimetable.xml.br";
    private const string FileName = "settings.xml";
    public static XmlSerializer XmlSerializer => new XmlSerializer(typeof(SettingsFile));

    public static SettingsFile Load(IFileReader fileReader)
    {
        if (!fileReader.FileExists(FileName)) return new SettingsFile();
        try
        {
            using var file = fileReader.ReadStream(FileName);
            return XmlSerializer.Deserialize(file) as SettingsFile;
        }
        catch (Exception e)
        {
            Debug.WriteLine(e);
            //ERROR READING
            return new SettingsFile();
        }
    }
    public static void Save(IFileReader fileReader, SettingsFile settings)
    {
        using var file = fileReader.WriteStream(FileName);
        XmlSerializer.Serialize(file,settings);
    }
}