using System.Diagnostics;
using System.IO.Compression;
using System.Xml;
using System.Xml.Serialization;
using Avalonia.Collections;

namespace STable.Shared;

public static class FieldGatherer
{ 
    public static HttpClient c = new();
   public static async Task<AvaloniaList<AvaloniaList<Field>>?>  GatherFieldsAsync(string url = "https://silverwebsiterepo.pages.dev/schooltimetable.xml.br")
   {
       var x = await GatherFieldsAsyncN(url);
       if (x is null) return null;
       AvaloniaList<AvaloniaList<Field>> y = new();
       foreach (var list in x)
       {
           y.Add(new(list));
       }

       return y;
   }
    public static async Task<List<List<Field>>?>  GatherFieldsAsyncN(string url = "https://silverwebsiterepo.pages.dev/schooltimetable.xml.br")
    {
        try
        {
            var get = await c.GetAsync(url);
            if (get.IsSuccessStatusCode)
            {
                await using var x = await get.Content.ReadAsStreamAsync();
                var s = new XmlSerializer(typeof(List<List<Field>>));
                if (get.Content.Headers.ContentType?.MediaType == "application/xml")
                {
                    return (List<List<Field>>?)s.Deserialize(XmlReader.Create(x));
                }
                await using var ux = new BrotliStream(x, CompressionMode.Decompress);
                return (List<List<Field>>?)s.Deserialize(XmlReader.Create(ux));
            }
        }
        catch (Exception e)
        {
            // ignored
            Debug.WriteLine(e.ToString());
        }
        return null;

    }
}