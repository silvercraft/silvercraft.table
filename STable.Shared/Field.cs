﻿using ReactiveUI;

namespace STable.Shared;
public class Field : ReactiveObject
{
    public Field()
    {
    }
    public Field(string name, string? roomID = null, bool bold = false, int? rowSpan = null, string extraText = null)
    {
        Name = name;
        RoomID = roomID;
        Bold = bold;
        RowSpan = rowSpan;
    }
    private string _Name = "";
    private string? _RoomID = null;
    private bool _Bold = false;
    private int? _RowSpan = null;
    private string? _ExtraText = null;
    public string ExtraText { get => _ExtraText; set => this.RaiseAndSetIfChanged(ref _ExtraText, value); }
    public string Name { get => _Name; set => this.RaiseAndSetIfChanged(ref _Name, value); }
    public string? RoomID { get => _RoomID; set => this.RaiseAndSetIfChanged(ref _RoomID, value); }
    public bool Bold { get => _Bold; set => this.RaiseAndSetIfChanged(ref _Bold, value); }
    public int? RowSpan { get => _RowSpan; set => this.RaiseAndSetIfChanged(ref _RowSpan, value); }
}